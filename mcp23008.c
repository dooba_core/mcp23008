/* Dooba SDK
 * MCP23008 I2C I/O Expander
 */

// External Includes
#include <string.h>
#include <i2c/i2c.h>

// Internal Includes
#include "regs.h"
#include "mcp23008.h"

// Set Direction
void mcp23008_set_dir(uint8_t addr, uint8_t dir)
{
	// Set Direction
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23008_REG_IODIR, dir);
}

// Set Pull-up
void mcp23008_set_pullup(uint8_t addr, uint8_t pullup)
{
	// Set Pull-up
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23008_REG_GPPU, pullup);
}

// Set GPIO
void mcp23008_set_gpio(uint8_t addr, uint8_t gpio)
{
	// Write GPIO
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23008_REG_GPIO, gpio);
}

// Get GPIO
uint8_t mcp23008_get_gpio(uint8_t addr)
{
	// Read GPIO
	if(addr == 0xff)														{ return 0; }
	return i2c_read_reg(addr, MCP23008_REG_GPIO);
}

// Register as DIO expander
void mcp23008_reg_dio_exp(struct mcp23008_dio_exp *e, uint8_t addr)
{
	// Setup Structure
	e->addr = addr;
	dio_reg_exp(&(e->exp), 8, e, (dio_exp_set_t)mcp23008_dio_set_dir, (dio_exp_set_t)mcp23008_dio_set_pullups, (dio_exp_set_t)mcp23008_dio_set_pins, (dio_exp_get_t)mcp23008_dio_get_pins);
}

// DIO Expander Interface - Set Direction
void mcp23008_dio_set_dir(struct mcp23008_dio_exp *e, uint32_t pins)
{
	// Set Direction
	mcp23008_set_dir(e->addr, ~pins);
}

// DIO Expander Interface - Set Pull-ups
void mcp23008_dio_set_pullups(struct mcp23008_dio_exp *e, uint32_t pins)
{
	// Set Pull-ups
	mcp23008_set_pullup(e->addr, pins);
}

// DIO Expander Interface - Set Pins
void mcp23008_dio_set_pins(struct mcp23008_dio_exp *e, uint32_t pins)
{
	// Set Pins
	mcp23008_set_gpio(e->addr, pins);
}

// DIO Expander Interface - Get Pins
uint32_t mcp23008_dio_get_pins(struct mcp23008_dio_exp *e)
{
	// Get Pins
	return mcp23008_get_gpio(e->addr);
}
