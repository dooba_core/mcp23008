/* Dooba SDK
 * MCP23008 I2C I/O Expander
 */

#ifndef	__MCP23008_REGS_H
#define	__MCP23008_REGS_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Registers
#define	MCP23008_REG_IODIR						0x00
#define	MCP23008_REG_GPPU						0x06
#define	MCP23008_REG_GPIO						0x09

#endif
