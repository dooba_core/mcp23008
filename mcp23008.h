/* Dooba SDK
 * MCP23008 I2C I/O Expander
 */

#ifndef	__MCP23008_H
#define	__MCP23008_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <dio/dio.h>

// DIO Expander Structure
struct mcp23008_dio_exp
{
	// Address
	uint8_t addr;

	// DIO Expander
	struct dio_exp exp;
};

// Set Direction
extern void mcp23008_set_dir(uint8_t addr, uint8_t dir);

// Set Pull-up
extern void mcp23008_set_pullup(uint8_t addr, uint8_t pullup);

// Set GPIO
extern void mcp23008_set_gpio(uint8_t addr, uint8_t gpio);

// Get GPIO
extern uint8_t mcp23008_get_gpio(uint8_t addr);

// Register as DIO expander
extern void mcp23008_reg_dio_exp(struct mcp23008_dio_exp *e, uint8_t addr);

// DIO Expander Interface - Set Direction
extern void mcp23008_dio_set_dir(struct mcp23008_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Set Pull-ups
extern void mcp23008_dio_set_pullups(struct mcp23008_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Set Pins
extern void mcp23008_dio_set_pins(struct mcp23008_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Get Pins
extern uint32_t mcp23008_dio_get_pins(struct mcp23008_dio_exp *e);

#endif
